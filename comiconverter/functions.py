#!/usr/bin/env python
import os

import shutil
import logging
from io import BytesIO
from PIL import Image
from shutil import copyfileobj

from zipfile import ZipFile
import tarfile

from tempfile import mkdtemp
import concurrent.futures as cf
import click

logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger(__name__)

extentionByFormat = {
    "JPEG": ".jpeg",
    "JPG": ".jpg",
    "WEBP": ".webp",
    "PNG": ".png"
}


def filename_from_path(path):
    return os.path.splitext(os.path.basename(path))[0]


def get_extention(file_name):
    return os.path.splitext(file_name)[1].lower()


def unpack_archive(path):
    tmp_dir = mkdtemp()
    file_extension = get_extention(path)

    if file_extension in ['.zip', '.cbz']:
        archive = ZipFile(path, 'r')
        namelist = archive.namelist()
    elif file_extension in [".tar", ".cbt"]:
        archive = tarfile.TarFile(path, 'r')
        namelist = archive.getnames()

    for member in namelist:
        filename = os.path.basename(member)
        # if directory break the loop
        if not filename:
            continue

        # extract each file
        source = archive.open(member)
        target = open(os.path.join(tmp_dir, filename), "wb")
        with source, target:
            copyfileobj(source, target)

    return tmp_dir


def convert_file(origin, image_format, resize, futur_file_name):
    try:
        im = Image.open(origin).convert("RGB")
        if resize[0]:
            im.thumbnail((int(resize[0]), int(resize[1])), Image.ANTIALIAS)

        tmpFile = BytesIO()
        im.save(tmpFile, image_format, quality=90)
        tmpFile.seek(0)

        info = tarfile.TarInfo(futur_file_name)
        info.size = len(tmpFile.getbuffer())
        return (info, tmpFile)

    except Exception as e:
        return str(e)


def convert_archive(working_directory, new_file_path, image_format="JPEG",
                    resize=None):

    with tarfile.open(new_file_path, "w") as tar_file:
        file_to_process = []

        # search all files in the extracted directory
        for dir_name, dir_list, file_list in os.walk(working_directory):
            for fileName in file_list:
                # if the file is exploitable we push it in list
                if get_extention(fileName) in extentionByFormat.values():
                    file_to_process.append(os.path.join(dir_name, fileName))

        # we convert every file

        inmemory_files = []
        nb_file = len(file_to_process)
        label = filename_from_path(new_file_path)

        with cf.ProcessPoolExecutor(max_workers=7) as executor,\
                click.progressbar(length=nb_file, label=label) as pb:

            futures = []
            for image_path in file_to_process:

                new_file_name = '%s%s' % (os.path.splitext(image_path)[
                                          0], extentionByFormat[image_format])

                # substract temp file path ex: /tmp/fesfene
                new_file_name = new_file_name.replace(
                    "%s/" % working_directory, "")

                task = executor.submit(convert_file, image_path,
                                       image_format, resize, new_file_name)

                futures.append(task)

            # 2.3. Consolidate result as a list and return this list.
            for future in futures:
                inmemory_files.append(future.result())
                pb.update(1)


        for f in inmemory_files:
            tar_file.addfile(f[0], f[1])


def launch(path="./", image_format="JPEG", resize=None, recursive=False):
    root_dir = os.getcwd()

    files_to_process = []

    # we walk directory topdown
    for dir_name, dir_list, file_list in os.walk(path):
        for file_name in file_list:
            # we check if each file is supported

            if get_extention(file_name) in ['.zip', '.cbz',
                                            '.tar', 'cbt']:
                # we rebuild the filename path
                files_to_process.append("%s/%s" % (dir_name, file_name))
        if not recursive:
            break

    for archive_path in files_to_process:
        logger.info("- Unpacking: %s" % archive_path)
        file_path = archive_path

        # unpacking archive
        unpacked_archive_path = unpack_archive(file_path)
        new_file_path = "%s.tar" % os.path.splitext(file_path)[0]

        convert_archive(unpacked_archive_path,
                        new_file_path, image_format, resize)

        # we clean unpacked directory
        shutil.rmtree(unpacked_archive_path)
        logger.info("- Finished: %s" % new_file_path)
